package uj.jwzp.exam2020.Service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uj.jwzp.exam2020.entity.Author;
import uj.jwzp.exam2020.entity.AuthorDTO;
import uj.jwzp.exam2020.entity.Book;
import uj.jwzp.exam2020.repository.AuthorRepositoryWithNoPersistence;
import uj.jwzp.exam2020.repository.BookRepositoryWithNoPersistence;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorService {

    private final AuthorRepositoryWithNoPersistence authorRepositoryWithNoPersistence;
    private final BookRepositoryWithNoPersistence bookRepositoryWithNoPersistence;
    private final ModelMapper modelMapper;

    @Autowired
    public AuthorService(AuthorRepositoryWithNoPersistence greetingRepositoryWithNoPersistence, BookRepositoryWithNoPersistence bookRepositoryWithNoPersistence, ModelMapper modelMapper) {
        this.authorRepositoryWithNoPersistence = greetingRepositoryWithNoPersistence;
        this.bookRepositoryWithNoPersistence = bookRepositoryWithNoPersistence;
        this.modelMapper = modelMapper;
    }

    public Optional<Author> getAuthor(long id) {
        return authorRepositoryWithNoPersistence.getById(id);
    }

    public List<Author> getAuthors() {
        return authorRepositoryWithNoPersistence.getAll();
    }

    public Author save(AuthorDTO authorDTO) {
        Author author = modelMapper.map(authorDTO, Author.class);
        authorRepositoryWithNoPersistence.save(author);
        return author;
    }

    public boolean delete(long id) {
        List<Book> books = bookRepositoryWithNoPersistence.getAll();
        Optional<Book> optionalBook = books.stream().filter(book -> book.getAuthor() == id).findFirst();
        if(optionalBook.isPresent()) return false;
        authorRepositoryWithNoPersistence.deleteById(id);
        return true;
    }
}
