package uj.jwzp.exam2020.Service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uj.jwzp.exam2020.entity.Author;
import uj.jwzp.exam2020.entity.Book;
import uj.jwzp.exam2020.entity.BookDTO;
import uj.jwzp.exam2020.repository.AuthorRepositoryWithNoPersistence;
import uj.jwzp.exam2020.repository.BookRepositoryWithNoPersistence;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookService {
    private final BookRepositoryWithNoPersistence bookRepositoryWithNoPersistence;
    private final AuthorRepositoryWithNoPersistence authorRepositoryWithNoPersistence;
    private final ModelMapper modelMapper;

    @Autowired
    public BookService(BookRepositoryWithNoPersistence bookRepositoryWithNoPersistence, AuthorRepositoryWithNoPersistence authorRepositoryWithNoPersistence, ModelMapper modelMapper) {
        this.bookRepositoryWithNoPersistence = bookRepositoryWithNoPersistence;
        this.authorRepositoryWithNoPersistence = authorRepositoryWithNoPersistence;
        this.modelMapper = modelMapper;
    }

    public Optional<Book> getBook(long id) {
        return bookRepositoryWithNoPersistence.getById(id);
    }

    public List<Book> getBooks(String byTitle) {

        if (byTitle == null) {
            return bookRepositoryWithNoPersistence.getAll();
        } else {
            List<Book> bookList = bookRepositoryWithNoPersistence.getAll();
            if (byTitle.length() < 2) {
                return null;
            }
            return bookList.stream().filter(book -> book.getTitle().toLowerCase().contains(byTitle.toLowerCase())).collect(Collectors.toList());
        }
    }

    public Optional<Book> save(BookDTO bookDTO) {
        Book book = modelMapper.map(bookDTO, Book.class);
        Optional<Author> author = authorRepositoryWithNoPersistence.getById(book.getAuthor());
        if (book.getAuthor() != 0 && author.isEmpty()) {
            return Optional.empty();
        }
        bookRepositoryWithNoPersistence.saveBook(book);
        return Optional.of(book);
    }

    public void delete(long id) {
        bookRepositoryWithNoPersistence.deleteById(id);
    }

}
