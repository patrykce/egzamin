package uj.jwzp.exam2020.entity;

public class Book {

    private long id;
    private String title;
    private long author;
    private int year;

    public Book() {
    }

    public Book(long id, String title, long author, int year) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.year = year;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public long getAuthor() {
        return author;
    }

    public void setAuthor(long author) {
        this.author = author;
    }
}
