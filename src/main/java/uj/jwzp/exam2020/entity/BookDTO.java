package uj.jwzp.exam2020.entity;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BookDTO {
    private final String title;
    private final Integer year;
    private final long author;


    @JsonCreator
    public BookDTO(@JsonProperty("title") String title,
                   @JsonProperty("year") Integer year,
                   @JsonProperty("author") long author) {
        this.title = title;
        this.author = author;
        this.year = year;
    }

    public Integer getYear() {
        return year;
    }

    public String getTitle() {
        return title;
    }

    public long getAuthor() {
        return author;
    }
}
