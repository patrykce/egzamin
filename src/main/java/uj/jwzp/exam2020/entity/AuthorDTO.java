package uj.jwzp.exam2020.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthorDTO {

    private final String name;
    private final String birthDate;
    private final String deathDate;

    @JsonCreator
    public AuthorDTO(@JsonProperty("name") String name,
                     @JsonProperty("birthDate") String birthDate,
                     @JsonProperty("deathDate") String deathDate) {
        this.name = name;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
    }

    public String getName() {
        return name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getDeathDate() {
        return deathDate;
    }
}
