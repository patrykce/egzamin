package uj.jwzp.exam2020.repository;

import org.springframework.stereotype.Repository;
import uj.jwzp.exam2020.entity.Book;

import java.util.*;

@Repository
public class BookRepositoryWithNoPersistence {

    private final Set<Book> books;
    private long counter;

    public BookRepositoryWithNoPersistence() {
        this.books = new HashSet<>();
        counter = 1;
    }

    public Optional<Book> getById(long id) {
        return books.stream().filter(b -> b.getId() == id).findFirst();
    }

    public List<Book> getAll() {
        return new ArrayList<>(books);
    }

    public boolean saveBook(Book book) {
        book.setId(counter);
        counter++;
        return books.add(book);
    }

    public void deleteById(long id) {
        Optional<Book> retrievedBook = books.stream().filter(b -> b.getId() == id).findFirst();
        retrievedBook.ifPresent(books::remove);
    }


}
