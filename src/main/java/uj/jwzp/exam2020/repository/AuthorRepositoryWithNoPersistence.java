package uj.jwzp.exam2020.repository;

import org.springframework.stereotype.Repository;
import uj.jwzp.exam2020.entity.Author;

import java.util.*;

@Repository
public class AuthorRepositoryWithNoPersistence {

    private final Set<Author> authors;
    private long counter;

    public AuthorRepositoryWithNoPersistence() {
        this.authors = new HashSet<>();
        counter = 1;
    }

    public Optional<Author> getById(long id) {
        return authors.stream().filter(b -> b.getId() == id).findFirst();
    }

    public List<Author> getAll() {
        return new ArrayList<>(authors);
    }

    public boolean save(Author author) {
        author.setId(counter);
        counter++;
        return authors.add(author);
    }

    public void deleteById(long id) {
        Optional<Author> retrievedBook = authors.stream().filter(b -> b.getId() == id).findFirst();
        retrievedBook.ifPresent(authors::remove);
    }

}
