package uj.jwzp.exam2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uj.jwzp.exam2020.Service.AuthorService;
import uj.jwzp.exam2020.entity.Author;
import uj.jwzp.exam2020.entity.AuthorDTO;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/authors", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class AuthorController {

    private final AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping()
    public ResponseEntity<List<Author>> getAll() {
        List<Author> authors = authorService.getAuthors();
        return new ResponseEntity<>(authors, HttpStatus.OK);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<Author> getById(@PathVariable("id") long id) {
        Optional<Author> result = authorService.getAuthor(id);
        return result.map(author -> new ResponseEntity<>(author, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping()
    public ResponseEntity<Author> save(@RequestBody AuthorDTO authorDTO) {
        Author book = authorService.save(authorDTO);
        return new ResponseEntity<>(book, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") long id) {
        boolean deleted = authorService.delete(id);
        if (!deleted) {
            return new ResponseEntity<>("cannot delete Author with id: " + id, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("deleted Author with id = " + id, HttpStatus.OK);
    }

}
