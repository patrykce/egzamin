package uj.jwzp.exam2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uj.jwzp.exam2020.Service.BookService;
import uj.jwzp.exam2020.entity.Book;
import uj.jwzp.exam2020.entity.BookDTO;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/books", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class BookController {
    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping()
    public ResponseEntity<List<Book>> getAll(@RequestParam(required = false) String byTitle) {
        List<Book> books = bookService.getBooks(byTitle);
        if(books == null ) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Book> getById(@PathVariable("id") long id) {
        Optional<Book> result = bookService.getBook(id);
        return result.map(book -> new ResponseEntity<>(book, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping()
    public ResponseEntity<Book> save(@RequestBody BookDTO bookDTO) {
        Optional<Book> result = bookService.save(bookDTO);
        return result.map(book -> new ResponseEntity<>(book, HttpStatus.CREATED)).orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") long id) {
        bookService.delete(id);
        return new ResponseEntity<>("deleted object with id = " + id, HttpStatus.OK);
    }
}
