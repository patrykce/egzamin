package uj.jwzp.exam2020.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {
    @RequestMapping(value = "/greeting", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody
    String greeting() {
        return "Hello World";
    }
}
